var options = {
  valueNames: [ 'devicename', 'vendor', 'lineage_version', 'mokee_version', 'e_version', 'pixelexperience_version', 'havoc_version', 'release', 'battery_repairscore', 'headphone_jack' ]
};

var filters = {};
var listObj = null;

function init() {
    if (listObj === null) {
        listObj = new List('androids', options);
    }
}

document.querySelector('#search-field').addEventListener('keyup', function(e) {
    init();
    var searchString = this.value;
    listObj.search(searchString);
});

document.querySelectorAll('select').forEach(function(select) {
    select.addEventListener('change', function(e) {
        init();
        if (e.target.selectedIndex === 0) {
            delete filters[select.dataset.target];
        } else {
            filters[select.dataset.target] = e.target.value;
        }
        listObj.filter(function(item) {
            for (var i=0; i < Object.keys(filters).length; i++) {
                if (item.values()[Object.keys(filters)[i]].trim() != Object.values(filters)[i]) {
                    return false;
                }
            }
            return true;
        });
    });
});