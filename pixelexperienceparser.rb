# Parse the PixelExperience devices json list

# https://github.com/PixelExperience/official_devices

require 'yaml'
require 'yaml/store'
require 'json'

db = YAML::Store.new("devices.yaml")

basedir = File.expand_path(File.dirname(__FILE__))
devicesfile = File.join(basedir, 'pixelexperience_devices', 'devices.json')

devices = JSON.load(File.new(devicesfile))

devices.each do |devicedata|
    currentversion = devicedata['supported_versions'].detect{|supported_version| supported_version['stable'] && ! supported_version['deprecated'] }
    next unless currentversion
    db.transaction do
        puts devicedata['codename']
        id = devicedata['codename']
        id = 'apollon' if devicedata['codename'] == 'apollo' # the m10t is not renamed
        devicedata['name'] = 'Xiaomi Mi 10 Lite Zoom' if devicedata['codename'] == 'vangogh' # Seems to be mislabeled
        db[id] = {} if db[id].nil?
        db[id]['vendor'] = devicedata['brand'] unless db[id]['brand']
        db[id]['name'] = devicedata['name'] unless db[id]['name']
        puts 'add release year manually!' unless db[id]['release']
        puts 'add battery data manually' unless db[id]['battery']
        version = case currentversion['version_code']
        when 'eleven' then 11.0
        when 'eleven_plus' then 11.0
        when 'twelve' then 12.0
        else
            puts "version not understood, was #{currentversion['version_code']}"
            next
        end
        
        db[id]['pixelexperience_version'] = version
    end
end