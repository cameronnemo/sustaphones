require 'yaml'
require 'yaml/store'
require 'json'
require 'httparty'
require 'addressable/uri'
require 'date'

# Parse the ifixitpage and check which phones are east to repair (overall repair score, battery changeable)

# available phones: https://www.ifixit.com/api/2.0/categories

# id, meta information, guide list, including difficulty rating: https://www.ifixit.com/api/2.0/categories/CATEGORYNAME

# Site-respecting download function with delay, error handlung and retry
def download(url)
    sleep(rand(2..4))
    try = 0
    url = Addressable::URI.parse(url).normalize.to_s
    begin
        response = HTTParty.get(url, {headers: {"User-Agent" => "Sustaphone/1.0"}})
        return response.body
    rescue Errno::EHOSTUNREACH
        sleep(rand(2..4))
        if try < 4
            try = try + 1
            retry
        end 
    end
    return nil
end

# Applies all the heuristics to guess the name ifixit uses for the device
def ifixitname(values)
    vendor = values['vendor']
    name = values['name']

    #vendor specific rules
    if (vendor == 'Samsung')
        # () suffixes that are not years have to be removed
        name = name.gsub(/\(.....+\)/, '').strip
        name = name.gsub(/\(LTE\)/, '').strip
        name = name.gsub(/\LTE/, '').strip
    end
    if (vendor == 'Motorola')
        # () suffixes that are not years have to be removed
        name = name.gsub(/\(.....+\)/, '').strip
    end
    if (vendor == 'LG')
        # () suffixes that are not years have to be removed
        name = name.gsub(/\(.+\)/, '').strip
    end
    if (vendor == 'Asus')
        # () suffixes that are not years have to be removed
        name = name.gsub(/\(.+\)/, '').strip
    end

    # device specific rules
    if name == 'Galaxy Nexus GSM'
        name = 'Galaxy Nexus'
    end
    if name == 'Galaxy S5 LTE' || name == 'Galaxy S5 -A'
        name = 'Galaxy S5'
    end
    if name == 'RAZR'
        name = 'Droid RAZR'
    end
    if name == 'RAZR MAXX'
        name = 'Droid RAZR MAXX'
    end
    if name == 'Shield Portable'
        name = 'Shield'
    end
    if name == 'Xperia Tablet Z Wi-Fi' || name == 'Xperia Z4 Tablet LTE'
        name = 'Xperia Tablet Z'
    end
    if name == 'One (M8)' || name == 'One (M8) Dual SIM'
        name = 'One M8'
    end
    if name == 'Galaxy Tab S 10.5 Wi-Fi' || name == 'Galaxy Tab S 10.5'
        vendor = ''
        name = 'Galaxy Tab S 10.5'
    end
    if name == 'One Max (GSM)'
        name = 'One Max'
    end
    if name == 'Nexus 4' || name == 'Nexus 5' || name == 'Nexus 5X' || name == 'Nexus 6' || name == 'Nexus 6P' || name == 'Nexus 10'
        vendor = ''
    end
    if name == 'Moto Z' || name == 'Moto X Play' || name == 'Moto G6 Plus' || name == 'Moto G4' || name == 'Moto E5 Plus'
        vendor = ''
    end
    if name == 'Nexus 7 2013 (Wi-Fi, Repartitioned)'
        vendor = ''
        name = 'Nexus 7'
    end
    if name == 'Galaxy Tab S 8.4 Wi-Fi'
        name = 'Galaxy Tab S 8.4'
    end
    if name == 'G Pad 7.0 (LTE)' || name == 'G Pad 7.0 WiFi'
        name = 'G Pad 7.0'
    end
    if name == 'Galaxy S4 -A'
        name = 'Galaxy S4'
    end
    if name == 'Zenfone 3'
        name = 'Zenfone 3 ZE552KL'
    end
    if name == 'Vibe K5 / K5 Plus'
        name = 'Vibe K5'
    end
    if name == 'Ascend Mate 2 4G'
        name = 'Ascend Mate 7'
    end
    if name == 'Galaxy S5  Duos'
        name = 'Galaxy S5'
    end
    if name == 'Redmi K20 / Mi 9T'
        name = 'Mi 9T'
    end
    if name == 'Redmi K20 Pro / Mi 9T Pro'
        name = 'Mi 9T Pro'
    end
    if name == 'Redmi Note 8 / 8T'
        name = 'Redmi Note 8T'
    end
    if name == 'Xperia Z4 Tablet WiFi'
        name = 'Xperia Z4 Tablet'
    end
    if name == 'Xperia Tablet Z LTE'
        name = 'Xperia Tablet Z'
    end
    if name == 'L90'
        name = 'Optimus L90'
    end
    if name == 'Aquaris M5'
        vendor = ''
    end
    if name == 'Nexus 9 (LTE)' || name == 'Nexus 9 (Wi-Fi)'
        vendor = ''
        name = 'Nexus 9'
    end
    if name == 'Nexus 7 (LTE, 2013 version)' || name == 'Nexus 7 (Wi-Fi, 2013 version)'
        vendor = ''
        name = 'Nexus 7'
    end
    if name == 'One Max (Verizon)'
        name = 'One Max'
    end
    if name == 'Moto G' || name == 'Moto G 4G'
        name = 'Moto G 1st Generation'
    end
    if name == 'Shield Android TV'
        name = 'Shield Android TV 1st Generation'
    end
    if name == 'One (Verizon)' || name == 'One (GSM)'
        name = 'One'
    end
    if name == 'FP2'
        name = '2'
    end
    if name == 'Moto G 2014' || name == 'Moto G 2014 LTE'
        name = 'Moto G 2nd Generation'
    end
    if name == 'Mi Note 3'
        name = 'Redmi Note 3'
    end
    if name == 'Galaxy J7 (2015)' || name == 'Galaxy J7'
        name = 'Galaxy J7'
    end
    if name == 'Galaxy Nexus LTE (Sprint)' || name == 'Galaxy Nexus LTE (Verizon)'
        vendor = 'Samsung'
        name = 'Galaxy Nexus'
    end
    if name == 'Honor 4x (China Telecom)' || name == 'Honor 4/4X (Unified)'
        name = 'Honor 4x'
    end
    if name == 'Moto E 2015' || name == 'Moto E 2015 LTE'
        name = 'Moto E 2nd Generation'
    end
    if name == 'Mi 6X'
        name = 'Mi A2'
    end
    if name == 'G3 S' || name == 'G3 Beat'
        name = 'G3 Vigor'
    end
    if name == 'G2 Mini'
        name = 'G2 Mini LTE'
    end
    if name == 'Mi Note 2'
        name = 'Redmi Note 2'
    end
    if name == 'FP3'
        name = '3'
    end
    if name == 'Moto E'
        name = 'Moto E 1st Generation'
    end
    if name == 'Moto X'
        name = 'Moto X 1st Generation'
    end
    if name == 'Moto X 2014'
        name = 'Moto X 2nd Generation'
    end
    if name == 'Moto G 2015' || name == 'Moto G3 Turbo'
        name = 'Moto G 3rd Generation'
    end
    if name == 'Nexus Player'
        vendor = 'Asus'
    end
    if name == 'Galaxy S III (AT&T)'
        name = 'Galaxy S III'
    end
    if name == 'Zenfone 2 Laser (720p)' || name == 'Zenfone 2 Laser/Selfie (1080p)'
        name = 'Zenfone 2 Laser'
    end
    if name == 'Moto X Pure Edition/Style (2015)'
        name = 'Moto X Pure Edition'
    end
    if name == 'Find 7a/s'
        name = 'X9000'
    end
    if name == 'Find 7a'
        name = 'X9000'
    end
    if name == 'Find 7s'
        name = 'X9000'
    end
    if name == 'One M9 (Verizon)' || name == 'One M9 (GSM)'
        name = 'One M9'
    end
    if name == 'AT&T Trek 2 HD'
        vendor = ''
        name = 'ATT Trek_HD'
    end
    if name == 'Galaxy Note 8.0 (GSM)'
        name = 'Galaxy Note 8.0'
    end
    if name == 'Galaxy Note 9'
        name = 'Galaxy Note9'
    end
    if name == 'Galaxy A3 (2016)' || name == 'Galaxy A3'
        name = 'Galaxy A3 2016'
    end
    if name == 'Essential PH-1'
        name = 'Phone'
    end
    if name == 'Galaxy Note Pro 12.2 Wi-Fi'
        name = 'Galaxy Note Pro 12.2'
    end
    if name == 'Galaxy Tab S2 9.7 Wi-Fi (2016)'
        name = 'Galaxy Tab S2 9.7 SM-T813'
    end
    if name == 'Galaxy Tab S2 8.0 Wi-Fi (2016)'
        name = 'Galaxy Tab S2 8.0'
    end
    if name == 'Photon Q 4G LTE'
        name = 'Photon 4G MB855'
    end
    if name == 'Galaxy Tab E 9.6 (WiFi)'
        name = 'Galaxy Tablet E 9.6 Wi-Fi'
    end
    if name == 'Galaxy Note 10.1 Wi-Fi (2014)'
        name = 'Galaxy_Note_10.1_2014'
    end
    if name == 'Galaxy J5N' || name == 'Galaxy J5' || name == 'Galaxy J5 3G'
        name = 'Galaxy J5 (2015)'
    end
    if name == 'Galaxy Note 2 (3G)'
        name = 'Galaxy Note II'
    end
    if name == 'One A9 (International GSM)'
        name = 'One A9'
    end
    if name == '9' && vendor == 'OnePlus'
        name = '9 5G'
    end
 
    return "#{vendor} #{name}".strip
end

devices = YAML.load_file('devices.yaml',  permitted_classes: [Date])
db = YAML::Store.new("devices.yaml")

devices.each do |id, values|
    url = 'https://www.ifixit.com/api/2.0/categories/' + ifixitname(values)
    puts url
    begin
        data = JSON.parse(download(url))
    rescue => e
        puts e
        puts "Found no API entry for #{url}"
        puts "Original name: #{values['name']}"
        puts '---'
        next
    end

    # The battery replacement guide was without subject for the Motorola Droid RAZR MAXX
    batteryguide = data['guides']&.detect{|x| (x['subject'] == 'Battery' || x['title'].include?('Battery Replacement') || x['subject'].include?('Battery Assembly')) && ! x['flags'].include?('GUIDE_ARCHIVED') }
    if batteryguide.nil?
        puts "Found no batteryguide for #{url}"
        puts "Original name: #{values['name']}"
        puts '---'
    end
    db.transaction do
        db[id]['battery_repairscore'] = batteryguide['difficulty'] unless batteryguide.nil?
        db[id]['battery_guide'] = batteryguide['url'] unless batteryguide.nil?
        db[id]['images'] = data['image'] if db[id]['images'].nil?
    end
end

