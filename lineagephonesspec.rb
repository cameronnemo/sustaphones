require 'yaml'
require 'yaml/store'
require 'httparty'
require 'addressable/uri'

# Collect headphone jack data from /u/nobodywasishere's spreadsheet LineageOS Phones by Spec


# Site-respecting download function with delay, error handlung and retry
def download(url)
    sleep(rand(2..4))
    try = 0
    url = Addressable::URI.parse(url).normalize.to_s
    begin
        response = HTTParty.get(url, {headers: {"User-Agent" => "Sustaphone/1.0"}})
        return response.body
    rescue Errno::EHOSTUNREACH
        sleep(rand(2..4))
        if try < 4
            try = try + 1
            retry
        end 
    end
    return nil
end

csv = CSV.new(download('https://docs.google.com/spreadsheets/d/1bx6RvTCEGn5zA06lW_uZGZ_dq6qQyCZC_NifmyeC1lM/gviz/tq?tqx=out:csv&sheet=Devices'), headers: true)
specs = {}
while line = csv.shift
    p line
    specs[line['Codename'].to_s] = line.to_h.keep_if{|k, _| k == 'HP Jack' }
end

devices = YAML.load_file('devices.yaml')
db = YAML::Store.new("devices.yaml")

devices.each do |id, values|
    headphoneJack = specs[id]['HP Jack'] if specs[id]
    db.transaction do
        db[id]['headphone_jack'] = headphoneJack unless headphoneJack.nil?
    end
end


