# Parse the /e/ site and write into the database which phones are supported

# https://gitlab.e.foundation/e/documentation/user/-/tree/master/_data/devices

require 'yaml'
require 'yaml/store'

# Parse the lineage wiki's yaml and write into the database which phones are supported

db = YAML::Store.new("devices.yaml")

basedir = File.expand_path(File.dirname(__FILE__))
devicesdir = File.join(basedir, 'e_docs', 'htdocs' ,'_data', 'devices')
Dir.children(devicesdir).each do |devicefile|
    devicedata = YAML.load_file(File.join(devicesdir, devicefile))
    # ignore split devices that are incorrectly combines. Like:
    #   Samsung Galaxy Tab 2 7.0 / Tab 2 10.1
    #   RAZR/RAZR MAXX (GSM)
    # They are corrected by hand in the yaml db
    # TODO: Still update their lineage version...
    if devicedata['codename'] == 'espressowifi' || devicedata['codename'] == 'espresso3g' || devicedata['codename'] == 'umts_spyder' || devicedata['codename'] == 'spyder' || devicedata['codename'] == 'cancro' || devicedata['codename'] == 'oneplus3' || devicedata['codename'] == 'find7'
        next
    end
    db.transaction do
        puts devicedata['codename']
        id = devicedata['codename']
        db[id] = {} if db[id].nil?
        db[id]['vendor'] = devicedata['vendor'] unless db[id]['vendor']
        db[id]['name'] = devicedata['name'] unless db[id]['name']
        db[id]['release'] = devicedata['release'] unless db[id]['release']
        db[id]['type'] = devicedata['type'] unless db[id]['type']
        db[id]['battery'] = devicedata['battery'] unless db[id]['battery']
        if devicedata['build_version_stable']
            e_version = devicedata['build_version_stable'].respond_to?('any?') ? devicedata['build_version_stable'].first : devicedata['build_version_stable']
        else
            e_version = devicedata['build_version_dev'].respond_to?('any?') ? devicedata['build_version_dev'].first : devicedata['build_version_dev']
        end
        e_version = 'q' if e_version.to_s == '17.1'
        e_version = e_version.downcase
        db[id]['e_version'] = e_version
    end
end
