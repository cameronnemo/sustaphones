# Parse the havoc devices json list

# https://github.com/Havoc-OS/official_devices

require 'yaml'
require 'yaml/store'

db = YAML::Store.new("devices.yaml")

basedir = File.expand_path(File.dirname(__FILE__))
devicesfile = File.join(basedir, 'havoc_devices', 'devices')

devices = File.new(devicesfile).read.lines

devices.each do |device|
    codename = device.strip
    codename = 'apollon' if codename == 'apollo' # apollo was not renamed in this upstream, referencing the Xiaomi Mi 10T (Pro)
    db.transaction do
        id = codename
        next if db[id].nil?  # havoc does not provide enough data here to add new device. We only extend existing entries
        db[id]['havoc_version'] = 11.0  # havoc currently only offers Android 11.0, calling it an LTS version
    end
end