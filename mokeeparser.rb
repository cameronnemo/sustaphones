require 'yaml'
require 'yaml/store'

# Parse the mookee deviced site and write into the database which phones are supported

db = YAML::Store.new('devices.yaml')

# https://download.mokeedev.com/devices.html
zoo = File.read('mokee_devices.html')
zoo = zoo.lines

def map_to_lineage_name(codename)
    case codename
    when 'z00d' then 'Z00D'
    when 'z00a' then 'Z00A'
    when 'z008' then 'Z008'
    when 'z00l' then 'Z00L'
    when 'z00t' then 'Z00T'
    when 'a6020' then 'A6020'
    when 'sakura' then 'twolip'
    else codename
    end
end

zoo.each_with_index do |line, index|
    if line.include?('<a href="/') && ! line.include?('Home')
        line = line.gsub('<a href="/', '')
        line = line.gsub(/.+\">/, '')
        line = line.gsub('</a><br>', '')
        codename = line.strip
        codename = map_to_lineage_name(codename)

        versionline = zoo[index + 3]
        version = versionline.gsub('<td>', '').gsub('</td>', '').strip

        db.transaction do
            if db[codename].nil?
                puts "unknown: #{codename}"
                next
            end
            db[codename]['mokee_version'] = version.to_f
        end
    end
end