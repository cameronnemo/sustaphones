require 'yaml'
require 'erb'
require 'tilt'
require 'date'

# Take the information from the database about which phones are repairable and supported by Roms and create the html of the page

def translate_difficulty(difficulty, battery_removable)
    return 1 if battery_removable

    case difficulty
    when 'Very easy' then 1
    when 'Easy' then 2
    when 'Moderate' then 3
    when 'Difficult' then 4
    when 'Very difficult' then 5
    else 6
    end
end

def translate_versions(lineage, mokee, e, pixel, havoc)
    lineage_version = case lineage
    when 19.1 then 12.0
    when 18.1 then 11.0
    when 17.1 then 10.0
    when 16.0 then 9.0
    when 15.1 then 8.1
    when 14.1 then 7.1
    when 13.0 then 6.0
    end
    e_version = case e
    when 'r' then 11.0
    when 'q' then 10.0
    when 'pie' then 9.0
    when 'oreo' then 8.1
    when 'nougat' then 7.1
    else 0
    end
    [lineage_version.to_f, mokee.to_f, e_version.to_f, pixel.to_f, havoc.to_f].max
end

def release_to_year(release)
    release.respond_to?('any?') ? release.first.values.first.to_s.gsub(/-.+/, '')  : release.to_s.gsub(/-.+/, '')
end

devices = YAML.load_file('devices.yaml', permitted_classes: [Date])
devices = devices.sort_by{|id, values| [
                                        values['battery'].respond_to?('has_key?') ? (values['battery']['removable'] ? 0 : 1) : 1 ,
                                        translate_difficulty(values['battery_repairscore'], values['battery'].respond_to?('has_key?') ? values['battery']['removable'] : false),
                                        - translate_versions(values['lineage_version'],values['mokee_version'], values['e_version'], values['pixelexperience_version'], values['havoc_version']),
                                        ( - release_to_year(values['release']).to_i),
                                        values['headphone_jack'] == 'Yes' ? 0 : 1 ,
                                        values['vendor']
                                        ]
                                        }

filters = {}
filters[:vendor] = devices.map{|id, x| x['vendor'] }.uniq.sort.reject{|x| x.nil? || x.empty? }
filters[:release] = devices.map{|id, x| release_to_year(x['release']) }.uniq.sort.reject{|x| x.nil? || x.empty? }.reverse
filters[:lineage_version] = devices.map{|id, x| x['lineage_version'].to_s }.uniq.sort.reject{|x| x.nil? || x.empty? }.reverse
filters[:mokee_version] = devices.map{|id, x| x['mokee_version'].to_s }.uniq.sort.reject{|x| x.nil? || x.empty? }
filters[:pixelexperience_version] = devices.map{|id, x| x['pixelexperience_version'].to_s }.uniq.sort.reject{|x| x.nil? || x.empty? }
filters[:e_version] = devices.map{|id, x| x['e_version'].to_s }.uniq.sort.reject{|x| x.nil? || x.empty? }.reverse
filters[:havoc_version] = devices.map{|id, x| x['havoc_version'].to_s }.uniq.sort.reject{|x| x.nil? || x.empty? }.reverse
filters[:battery_repairscore] = devices.map{|id, x| x['battery_repairscore'].to_s }.uniq.reject{|x| x.nil? || x.empty? }
filters[:headphone_jack] = devices.map{|id, x| x['headphone_jack'].to_s }.uniq.reject{|x| x.nil? || x.empty? }

basedir = File.expand_path(File.dirname(__FILE__))
view = File.join(basedir, 'views', 'index.erb')

layoutpath = File.join(basedir, 'views', 'layout.erb')
layout = Tilt::ERBTemplate.new(layoutpath)

context = Object.new    # this seems to be the only working way to make variables accessible in the layout
context.instance_variable_set(:@devices, devices)
context.instance_variable_set(:@filters, filters)

output = layout.render(context) {
    # This construction ensures the layout is used
    template = Tilt::ERBTemplate.new(view)
    # The render of the template inside the layout has nothing to evaluate. But it needs the
    # the local collection, thus it gets an empty Object as first param
    template.render(context)
}

targetpath = File.join(basedir, 'html', 'index.html')
File.write(targetpath, output)